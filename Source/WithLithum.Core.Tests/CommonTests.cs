// WithLithum.Core Series
// (C) WithLithum 2021, 2022.
// Licensed under GNU Lesser GPL 3.0 or later.

namespace WithLithum.Core.Tests;

using WithLithum.Core.Util;
using Xunit;

public class CommonTests
{
    [Fact]
    public void NamespacedIdTest()
    {
        var id = NamespaceId.Parse("test:testDirectory/testId");
        Assert.True(id.Validate());

        var test2 = new NamespaceId
        {
            Namespace = "//assadasd",
            Id = "../asdsad"
        };

        Assert.DoesNotMatch(NamespaceId.IdValidator, test2.Id);
        Assert.DoesNotMatch(NamespaceId.NamespaceValidator, test2.Namespace);
        Assert.False(test2.Validate());

        var id2 = NamespaceId.Parse("test:testDirectory/testId/testId2");
        Assert.True(id2.Validate());
    }
}
﻿namespace WithLithum.Core.Data.Packaging;
public class PackageManifest
{
    public string Id { get; set; } = "";
    public string Version { get; set; } = "";
}

﻿namespace WithLithum.Core.Windows.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/// <summary>
/// An enumeration of all possible offline statuses for file.
/// </summary>
public enum OfflineStatus
{
    /// <summary>
    /// If the file is open, it is open in the cache.
    /// </summary>
    Local = 1,
    /// <summary>
    /// If the file is open, it is open on the server.
    /// </summary>
    Remote,
    /// <summary>
    /// The file is available both on server and in the cache.
    /// </summary>
    Both,
    /// <summary>
    /// The local copy is currently incomplete. The file cannot be opened in offline mode until it has been synchronized.
    /// </summary>
    Incomplete
}

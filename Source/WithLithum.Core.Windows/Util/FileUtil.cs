﻿namespace WithLithum.Core.Windows.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

public static class FileUtil
{
    /// <summary>
    /// Determines whether the specified path points to an executable, a batch file
    /// or a script.
    /// </summary>
    /// <param name="path">The path.</param>
    /// <returns><see langword="true"/> if the path points to an executable, batch file or script;
    /// otherwise, <see langword="false"/>.</returns>
    public static bool IsExecutable(string path)
    {
        if (!File.Exists(path)) return false;

        return Shell32.PathIsExe(path);
    }

    /// <summary>
    /// Gets the offline status of a file.
    /// </summary>
    /// <param name="path">The path.</param>
    /// <returns>An enumeration represents the status.</returns>
    /// <exception cref="ArgumentNullException">Specified path is not specified.</exception>
    public static OfflineStatus GetOfflineStatus(string path)
    {
        if (path == null) throw new ArgumentNullException(nameof(path));

        return (OfflineStatus)(int)InternalGetOfflineStatus(path);
    }

    private static Shell32.OFFLINE_STATUS InternalGetOfflineStatus(string path)
    {
        var result = Shell32.SHIsFileAvailableOffline(path, out var status);

        if (result == HRESULT.E_INVALIDARG)
        {
            throw new ArgumentException("The path is invalid or not a network path; or the file or directory is not cached."
                , nameof(path));
        }

        if (result == HRESULT.E_FAIL)
        {
            throw new ArgumentException("The file or directory is not cached.", nameof(path));
        }

        return status;
    }
}

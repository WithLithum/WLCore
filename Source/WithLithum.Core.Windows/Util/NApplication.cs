﻿namespace WithLithum.Core.Windows.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

/// <summary>
/// Provides support for applications.
/// </summary>
public static class NApplication
{
    /// <summary>
    /// Creates a console for the current process.
    /// </summary>
    /// <returns><c>-1</c> if success; otherwise, returns GetLastError value.</returns>
    public static int CreateConsole()
    {
        if (!Kernel32.AllocConsole())
        {
            return Marshal.GetLastPInvokeError();
        }

        return -1;
    }

    /// <summary>
    /// Detaches this process from the console that is currently attached to.
    /// </summary>
    public static void DetachFromConsole()
    {
        Kernel32.FreeConsole();
    }
}

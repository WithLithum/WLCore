﻿namespace WithLithum.Core.Windows.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public static class DirectoryUtil
{
    /// <summary>
    /// Removes the localized name for the specified directory.
    /// </summary>
    /// <param name="path">Path to the directory.</param>
    /// <returns><see langword="true"/> if success; otherwise, <see langword="false"/>.</returns>
    public static bool RemoveLocalizedName(string path)
    {
        return Shell32.SHRemoveLocalizedName(path) == HRESULT.S_OK;
    }
}

﻿namespace WithLithum.Core.Windows.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public static class UserInput
{
    /// <summary>
    /// Gets or sets the maximum interval between two clicks allowed for the
    /// operating system to consider it as a double-click.
    /// </summary>
    /// <value>
    /// The double click interval. Maximum is <c>5000</c>; if set to larger
    /// than 5000, it is set to 5000 by OS.
    /// </value>
    /// <remarks>
    /// This method alters the settings for <i>all windows</i>. Do not use
    /// unless the user is aware of that this changes the global system config.
    /// </remarks>
    public static int DoubleClickInterval
    {
        get => (int)User32.GetDoubleClickTime();
        set => User32.SetDoubleClickTime((uint)value);
    }
}

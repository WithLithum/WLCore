﻿namespace WithLithum.Core.Windows;

using WithLithum.Core.Util;

public class UserWindow : IHandleable<IntPtr>
{
    public UserWindow(IntPtr handle)
    {
        Handle = handle;
    }

    public IntPtr Handle { get; }

    /// <summary>
    /// Gets or sets a value indicating whether this instance is enabled for mouse and
    /// keyboard input.
    /// </summary>
    public bool IsEnabled
    {
        get => User32.IsWindowEnabled(Verify().Handle);
        set => User32.EnableWindow(Verify().Handle, value);
    }

    /// <inheritdoc />
    /// <remarks>
    /// A thread should not validate window that it did not create, because the OS
    /// recycles the handles and this instance may no longer represent the instance
    /// that it is intended to represent at any time.
    /// </remarks>
    public bool IsValid()
    {
        return User32.IsWindow(Handle);
    }

    private UserWindow Verify()
    {
        if (!IsValid()) throw new InvalidOperationException("Window invalid.");
        return this;
    }

    /// <summary>
    /// Destroys this instance.
    /// </summary>
    /// <remarks>
    /// The function sends WM_DESTROY and WM_NCDESTROY messages to the window
    /// to deactivate it and remove the keyboard focus from it. The function
    /// also destroys the window's menu, flushes the thread message queue,
    /// destroys timers, removes clipboard ownership, and breaks the clipboard
    /// viewer chain (if the window is at the top of the viewer chain).
    /// </remarks>
    /// <exception cref="InvalidOperationException">The window is invalid.</exception>
    public void Destroy()
    {
        if (!IsValid()) throw new InvalidOperationException("Window invalid.");
        User32.DestroyWindow(Handle);
    }

    public static explicit operator UserWindow(NativeWindow window)
    {
        return new UserWindow(window.Handle);
    }
}

﻿namespace WithLithum.Core.Identity;

using System.Runtime.Versioning;
using Tmds.Linux;

[SupportedOSPlatform("linux")]
public class PosixUser
{
    internal PosixUser(uint id)
    {
        UserId = id;
    }

    public uint UserId { get; }
    public static PosixUser EffectiveUser => new(LibC.geteuid());

    public static PosixUser CurrentUser => new(LibC.getuid());
}

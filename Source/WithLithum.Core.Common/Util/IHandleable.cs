﻿// WithLithum.Core Series
// (C) WithLithum 2021, 2022.
// Licensed under GNU Lesser GPL 3.0 or later.

namespace WithLithum.Core.Util;

public interface IHandleable<out T>
{
    T Handle { get; }

    /// <summary>
    /// Determines that whether this instance still represents a valid instance in
    /// the operating system or it's environment if not the OS.
    /// </summary>
    /// <returns><see langword="true"/> if the handle is valid; otherwise, <see langword="false"/>.</returns>
    bool IsValid();
}

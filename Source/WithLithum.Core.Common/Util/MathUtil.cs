﻿// WithLithum.Core Series
// (C) WithLithum 2021, 2022.
// Licensed under GNU Lesser GPL 3.0 or later.

namespace WithLithum.Core.Util;

/// <summary>
/// Provides utilties for Math.
/// </summary>
public static class MathUtil
{
    /// <summary>
    /// Calculates a sigma notation.
    /// </summary>
    /// <param name="top">The top part.</param>
    /// <param name="bottom">The bottom part.</param>
    /// <param name="multiplier">The multiplier.</param>
    /// <returns>The final result.</returns>
    public static double Sigma(int top, int bottom, int multiplier)
    {
        double result = 0;

        for (var i = top; i <= bottom; i++)
        {
            result += i * multiplier;
        }

        return result;
    }
}

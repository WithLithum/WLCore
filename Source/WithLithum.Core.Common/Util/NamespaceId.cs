﻿// WithLithum.Core Series
// (C) WithLithum 2021, 2022.
// Licensed under GNU Lesser GPL 3.0 or later.

namespace WithLithum.Core.Util;
using System;
using System.Text.RegularExpressions;

/// <summary>
/// Represents a Minecraft-like namespaced id (<c>namespace:directory/id</c>).
/// </summary>
public class NamespaceId
{
    public static readonly Regex NamespaceValidator = new(@"^[\w+]");
    public static readonly Regex IdValidator = new(@"^[\w+(\/\w+)]");
    public static readonly Regex ParseValidator = new(@"^[\w]+:[\w+(\/\w+)]+");

    /// <summary>
    /// Gets or sets the identifier (with directories) of this instance.
    /// </summary>
    public string Id { get; set; } = "";

    /// <summary>
    /// Gets or sets the namespace of this instance.
    /// </summary>
    public string Namespace { get; set; } = "default";

    /// <summary>
    /// Determines whether this instance is valid.
    /// </summary>
    /// <returns>
    /// <see langword="true"/> if this instance is valid; otherwise, <see langword="false"/>.
    /// </returns>
    public bool Validate()
    {
        return NamespaceValidator.IsMatch(Id) && NamespaceValidator.IsMatch(Namespace);
    }

    /// <summary>
    /// Parses a <see cref="string"/> value into it's valid <see cref="NamespaceId"/> equivalent.
    /// </summary>
    /// <param name="input">The input.</param>
    /// <returns>An instance of <see cref="NamespaceId"/>.</returns>
    /// <exception cref="FormatException">The input format was invalid.</exception>
    public static NamespaceId Parse(string input)
    {
        if (!ParseValidator.IsMatch(input))
        {
            throw new FormatException("Invalid input format");
        }

        var splitted = input.Split(':');
        return new NamespaceId
        {
            Namespace = splitted[0],
            Id = splitted[1]
        };
    }
}

﻿// WithLithum.Core Series
// (C) WithLithum 2021, 2022.
// Licensed under GNU Lesser GPL 3.0 or later.

namespace WithLithum.Core.IO;

public static class FileUtil
{
    /// <summary>
    /// Truncates the specified file precisely to the specified length.
    /// </summary>
    /// <remarks>
    /// If the given value is less than the current length of the file, the extra data is lost.
    /// If the given value is larger than the current length of the file, the file is expanded.
    /// If the file is expanded, the contents of the file between the old and the new length are
    /// undefined on Windows, while on Linux, that space is filled with zeros, or null bytes
    /// (<c>\0</c>).
    /// <para>
    /// Additional set of exceptions may be thrown. See <see cref="Stream.SetLength(long)"/> for
    /// more information.
    /// </para>
    /// </remarks>
    /// <param name="filePath">The path to the file to truncate.</param>
    /// <param name="length">The length of the file.</param>
    /// <exception cref="ArgumentNullException">The <paramref name="filePath"/> was null.</exception>
    /// <exception cref="FileNotFoundException">The file was not found.</exception>
    /// <seealso cref="Stream.SetLength(long)"/>
    public static void Truncate(string filePath, long length)
    {
        if (filePath == null) throw new ArgumentNullException(nameof(filePath));

        if (!File.Exists(filePath))
        {
            throw new FileNotFoundException(null, filePath);
        }

        var stream = new FileStream(filePath, FileMode.Open, FileAccess.ReadWrite);
        stream.SetLength(length);
        stream.Close();
    }

    /// <summary>
    /// Truncates the specified file precisely to the specified length.
    /// </summary>
    /// <remarks>
    /// If the given value is less than the current length of the file, the extra data is lost.
    /// If the given value is larger than the current length of the file, the file is expanded.
    /// If the file is expanded, the contents of the file between the old and the new length are
    /// undefined on Windows, while on Linux, that space is filled with zeros, or null bytes
    /// (<c>\0</c>).
    /// <para>
    /// Additional set of exceptions may be thrown. See <see cref="Stream.SetLength(long)"/> for
    /// more information.
    /// </para>
    /// </remarks>
    /// <param name="file">The file to truncate.</param>
    /// <param name="length">The length of the file.</param>
    /// <exception cref="ArgumentNullException">The <paramref name="file"/> was null.</exception>
    /// <exception cref="FileNotFoundException">The file was not found.</exception>
    /// <seealso cref="Stream.SetLength(long)"/>
    public static void Truncate(FileInfo file, long length)
    {
        Truncate(file.FullName, length);
    }
}

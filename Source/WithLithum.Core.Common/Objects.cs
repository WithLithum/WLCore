﻿// WithLithum.Core Series
// (C) WithLithum 2021, 2022.
// Licensed under GNU Lesser GPL 3.0 or later.

namespace WithLithum.Core;

/// <summary>
/// Provides various utilities to deal with various types of POCOs, including
/// null and various other states.
/// </summary>
public static class Objects
{
    /// <summary>
    /// Ensures that the <paramref name="input"/> is not <see langword="null"/>.
    /// </summary>
    /// <typeparam name="T">The type of the input parameter.</typeparam>
    /// <param name="input">The input.</param>
    /// <param name="name">The name.</param>
    /// <returns>If not <see langword="null"/>, the <paramref name="input"/>.</returns>
    /// <exception cref="ArgumentNullException">The <paramref name="input"/> was <see langword="null"/>.</exception>
    public static T RequireParamNonNull<T>(T input, string name)
    {
        return input.ArgNotNull(name);
    }

    /// <summary>
    /// Ensures <paramref name="input"/> is not <see langword="null"/>.
    /// </summary>
    /// <typeparam name="T">The type of the input parameter.</typeparam>
    /// <param name="input">The input.</param>
    /// <param name="name">The name of the parameter.</param>
    /// <returns>If not <see langword="null"/>, the <paramref name="input"/>.</returns>
    /// <exception cref="ArgumentNullException">The <paramref name="input" /> was <see langword="null"/>.</exception>
    public static T ArgNotNull<T>(this T input, string name)
    {
        return input ?? throw new ArgumentNullException(name);
    }
}

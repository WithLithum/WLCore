# WithLithum Core Series

This project includes several reusable code components of WithLithum's MSBuild projects.

## Components

- [ ] Common
- [ ] _Win32 (`Windows`)_
- [ ] Data
- [ ] Identity

Componenst in italic are either not fully managed or they are platform dependent.

## Dependencies

Dependencies are managed through NuGet, but some requirements of
specific projects are below:

### WinUI

* As least Windows 10, version 1809 (Windows 11 will work perfectly too)
* Windows App SDK runtime installed on your computer
* Windows App SDK extension installed
* Visual Studio 2022, with .NET and UWP workload

Linux will not work here because WinUI is not for Linux.

### Linux

It will build on all platforms, but it will only run on Linux with a C library installed.

I recommend you use GNU/Linux distributions as it comes with `glibc` which fulfills this requirement (or as least you may get them in a package manager).

For *nix they may work but I cannot gurrantee, but if they use `so` format and if .NET works on them it will do.

If you are on Windows, WSL 2 will work fine, too.

### Win32

It will build on all platforms, but it will only run on Windows / compatibles with a **reasonable** set of Windows APIs.

ReactOS does not work because .NET does not work on it, and Wine is not tested.

## License

TL;DR: LGPL-3.0-or-later

This project is licensed under GNU [Lesser](COPYING-LESSER) [General Public License](COPYING) version 3, but you may choose any later version of either GPLv3 or LGPLv3.
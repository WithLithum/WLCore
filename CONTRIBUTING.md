# Contributing

Yep, this is a guide for setting up a development environment and contributing.

Please note that contributing on Mac is **well not supported**. You may need either a virtual machine or contribute at your own risk.

## Development Environment

Things different when they comes to platforms. I will use my knowlegde on most of these platforms`
to cover these.

### Windows

* You will need [.NET 6 SDK](https://dot.net/), but either [Visual Studio](https://visualstudio.com) and [Visual Studio](https://code.visualstudio.com) may be needed as they comes in handy when writing .NET Code.
* To use WinUI project you will also need [the Windows App SDK](https://docs.microsoft.com/windows/apps/windows-app-sdk/downloads).
* To test Linux APIs you will need to install WSL 2 and a GNU/Linux distribution on it. I recommend Ubuntu.
* Git for Windows.

### Linux

Setting a .NET development is a bit complicated here so I will demonstrate this by Ubuntu 20.04, but you are free to
read the official manuals.

#### Adding the package registries

Run the following code in your terminal:

```sh
wget https://packages.microsoft.com/config/ubuntu/20.04/packages-microsoft-prod.deb -O packages-microsoft-prod.deb
sudo dpkg -i packages-microsoft-prod.deb
rm packages-microsoft-prod.deb
```

That should install Microsoft registries to your computer.

#### Installing dependencies

```sh
sudo apt install -y apt-transport-https
sudo apt-get update
sudo apt-get install -y dotnet-sdk-6.0
```

After that, you are good to go. If you fails, refers to the [official documentation](https://docs.microsoft.com/dotnet/core/install/linux-ubuntu#2004-).

## Building the project

### Windows only

On Windows, you can use Visual Studio, or .NET CLI.

#### Visual Studio

* Open the solution file under `Source` folder with Visual Studio.
* Click Build -> Build Solution.

### All platforms

#### .NET CLI

* Open the Source folder with Windows Terminal or other terminal apps.
* Run `dotnet restore` and then `dotnet build`.